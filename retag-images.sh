#!/bin/bash

# Source info
SRC_REG_URL="registry.gitlab.com/tshayan1/imagebuild"
SRC_REPO_TOKEN="glpat-bJ4y28GuAcmo6xjMhwUD"
SRC_RETAG_LIST=(grafana:latest metricbeat:latest docker:dind)

# Destination info
DST_REG_URL=$SRC_REG_URL
DST_REPO_TOKEN=$SRC_REPO_TOKEN
DST_REG_USER=$CI_REGISTRY_USER
DST_REG_PASSWORD=$CI_REGISTRY_PASSWORD
DST_RETAG_LIST=(graafaanaa:latest metricbeat:new shayan-docker:neww)

# Main
for index in "${!SRC_RETAG_LIST[@]}"
do
  src_image=${SRC_RETAG_LIST[$index]}
  dst_image=${DST_RETAG_LIST[$index]}
  docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  docker pull $SRC_REG_URL/$src_image
  docker tag $SRC_REG_URL/$src_image $DST_REG_URL/$dst_image
  if [ "$DST_REG_USER" != "$CI_REGISTRY_USER" ]; then
    docker login -u $DST_REG_USER -p $DST_REG_PASSWORD $DST_REG_URL
  fi
  docker push $DST_REG_URL/$dst_image
done