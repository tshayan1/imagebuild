from .vnfd import onboard
from .cnfd import onboard
from .nsd import onboard, instantiate