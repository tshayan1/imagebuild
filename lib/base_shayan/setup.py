from setuptools import find_packages, setup
setup(
    name='shayan',
    packages=find_packages(include=['shayan']),
    version='0.1.0',
    description='My first Python library',
    author='Me',
    license='MIT',
    install_requires=[],
)