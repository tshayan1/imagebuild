import requests
import os
from datetime import datetime, timedelta

GITLAB_BASE_URL = "https://gitlab.com/api/v4/projects"
GITLAB_ACCESS_TOKEN = "glpat-bJ4y28GuAcmo6xjMhwUD"
GITLAB_PROJECT_ID = "43892679"

def upload_to_gitlab(file_path, package_name):
    filename = file_path.split("/")[-1]
    upload_url = f"{GITLAB_BASE_URL}/{GITLAB_PROJECT_ID}/packages/generic/{package_name}/default/{filename}?select=package_file"
    headers = {
    "PRIVATE-TOKEN": GITLAB_ACCESS_TOKEN
    }
    files = {"file": open(file_path, "rb")}
    response = requests.put(upload_url, headers=headers, files=files)
    return response.json()

def delete_old_backup_files(package_id, retention_days):
    retention_start_date = datetime.utcnow() - timedelta(days=retention_days)
    get_files_url = f"{GITLAB_BASE_URL}/{GITLAB_PROJECT_ID}/packages/{package_id}/package_files"
    headers = {
    "PRIVATE-TOKEN": GITLAB_ACCESS_TOKEN
    }  
    response = requests.get(get_files_url, headers=headers)
    files = response.json()
    if len(files) > retention_days:
        for file in files:
            if datetime.fromisoformat(file["created_at"][:-1]) < retention_start_date:
                delete_url = f"{GITLAB_BASE_URL}/{GITLAB_PROJECT_ID}/packages/{package_id}/package_files/{file['id']}"
                response = requests.delete(delete_url, headers=headers)

# Main
result = upload_to_gitlab("/tmp/test.tar.gz", "test-instance")
delete_old_backup_files(result["package_id"], 7)