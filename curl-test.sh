#!/bin/bash

LAB_REPO_TOKEN="glpat-bJ4y28GuAcmo6xjMhwUD"
LAB_REPO_API_URL="https://gitlab.com/api/v4/projects/43892679/registry/repositories"
PROD_REPO_TOKEN="glpat-bJ4y28GuAcmo6xjMhwUD"
PROD_REPO_API_URL="https://gitlab.com/api/v4/projects/43947350/registry/repositories"
LAB_IMAGES=( $( \
    for id in `curl \
                -s \
                --header "PRIVATE-TOKEN: glpat-bJ4y28GuAcmo6xjMhwUD" \
                "https://gitlab.com/api/v4/projects/43947350/registry/repositories" \
                | jq '.[].id' \
                | sed 's/"//g'`; \
        do \
            curl \
                -s \
                --header "PRIVATE-TOKEN: glpat-bJ4y28GuAcmo6xjMhwUD" \
                "https://gitlab.com/api/v4/projects/43947350/registry/repositories/$id/tags" \
                | jq '.[].path' \
                | sed 's/"//g' \
                | rev \
                | cut -d '/' -f1 \
                | rev; \
    done\
    ) )
PROD_IMAGES=( $(curl -s --header "PRIVATE-TOKEN: $PROD_REPO_TOKEN " "$PROD_REPO_API_URL" | jq '.[].name' | sed 's/"//g') )
PROD_REGISTRY_USER="tshayan-admin"
PROD_REGISTRY_PASSWORD="glpat-bJ4y28GuAcmo6xjMhwUD"
PROD_REGISTRY_URL="registry.gitlab.com/tshayanprod/imagebuild"

for lab_image in "${LAB_IMAGES[@]}"
do
  if [[ "${PROD_IMAGES[*]}" =~ "$lab_image" ]]; then
    continue
  else
    echo $lab_image
  fi
done