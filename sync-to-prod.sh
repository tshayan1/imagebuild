#!/bin/bash

# Lab info
LAB_REG_URL="registry.gitlab.com/tshayan1/imagebuild"
LAB_REPO_TOKEN="glpat-bJ4y28GuAcmo6xjMhwUD"
LAB_REPO_API_BASE_URL="https://gitlab.com/api/v4/projects"
LAB_PROJECT_ID="43892679"
LAB_IMAGES=( $( \
    for id in `curl \
                -s \
                --header "PRIVATE-TOKEN: $LAB_REPO_TOKEN" \
                "$LAB_REPO_API_BASE_URL/$LAB_PROJECT_ID/registry/repositories" \
                | jq '.[].id' | sed 's/"//g'`; \
        do \
            curl \
                -s \
                --header "PRIVATE-TOKEN: $LAB_REPO_TOKEN" \
                "$LAB_REPO_API_BASE_URL/$LAB_PROJECT_ID/registry/repositories/$id/tags" \
                | jq '.[].path' | sed 's/"//g' | rev | cut -d '/' -f1 | rev; \
    done\
    ) )
# Prod info
PROD_REG_URL="registry.gitlab.com/tshayanprod/imagebuild"
PROD_REG_USER="tshayan-admin"
PROD_REG_PASSWORD="glpat-bJ4y28GuAcmo6xjMhwUD"
PROD_REPO_TOKEN="glpat-bJ4y28GuAcmo6xjMhwUD"
PROD_REPO_API_BASE_URL="https://gitlab.com/api/v4/projects"
PROD_PROJECT_ID="43947350"
PROD_IMAGES=( $( \
    for id in `curl \
                -s \
                --header "PRIVATE-TOKEN: $PROD_REPO_TOKEN" \
                "$PROD_REPO_API_BASE_URL/$PROD_PROJECT_ID/registry/repositories" \
                | jq '.[].id' | sed 's/"//g'`; \
        do \
            curl \
                -s \
                --header "PRIVATE-TOKEN: $PROD_REPO_TOKEN" \
                "$PROD_REPO_API_BASE_URL/$PROD_PROJECT_ID/registry/repositories/$id/tags" \
                | jq '.[].path' | sed 's/"//g' | rev | cut -d '/' -f1 | rev; \
    done\
    ) )

# Main
for lab_image in "${LAB_IMAGES[@]}"
do
  if [[ "${PROD_IMAGES[@]}" =~ "$lab_image" ]]; then
    continue
  else
    image_name=$(echo $lab_image | cut -d ':' -f1)
    image_tag=$(echo $lab_image | cut -d ':' -f2)
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    docker pull $LAB_REG_URL/$image_name:$image_tag
    docker tag $LAB_REG_URL/$image_name:$image_tag $PROD_REG_URL/$image_name:$image_tag
    docker login -u $PROD_REG_USER -p $PROD_REG_PASSWORD $PROD_REG_URL
    docker push $PROD_REG_URL/$image_name:$image_tag
  fi
done